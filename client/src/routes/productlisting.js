import React from 'react';
import { Route } from 'react-router-dom';
import { List, Create, Update, Show } from '../components/productlisting/';

export default [
  <Route path="/product_listings/create" component={Create} exact key="create" />,
  <Route path="/product_listings/edit/:id" component={Update} exact key="update" />,
  <Route path="/product_listings/show/:id" component={Show} exact key="show" />,
  <Route path="/product_listings/" component={List} exact strict key="list" />,
  <Route path="/product_listings/:page" component={List} exact strict key="page" />
];
