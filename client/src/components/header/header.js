import React from "react";

import { Link, Redirect } from "react-router-dom";
const HeaderBlock = () => {
  return (
    <div>
      <div id="desktop">
        <div className="top-strip">
          <div className="container">
            <div className="row">
              <div className="col-sm-6 left">
                <img
                  src="https://nualoha.com/img/van.png"
                  className="img-fluid"
                  alt="van"
                />{" "}
                Free Shipping & Returns
              </div>
              <div className="col-sm-6 text-right">
                <span className="contact">
                  {" "}
                  <img
                    src="https://nualoha.com/img/call.png"
                    className="img-fluid"
                    alt="call"
                  />{" "}
                  <a className="emailLink" href="mailto:support@nualoha.com">
                    support@nualoha.com
                  </a>
                </span>
                <span className="social">
                  <a target="blank" href="https://twitter.com/NualohaNaturals">
                    <img
                      src="https://nualoha.com/img/twiteer.png"
                      className="img-fluid"
                      alt="twitter"
                    />
                  </a>
                  <a
                    target="blank"
                    href="https://www.facebook.com/nualohanaturals/"
                  >
                    <img
                      src="https://nualoha.com/img/facebook-logo-in-circular-button-outlined-social-symbol.png"
                      className="img-fluid"
                      alt="facebook"
                    />
                  </a>
                  <a target="blank" href="https://www.instagram.com/nualoha7/">
                    <img
                      src="https://nualoha.com/img/insta.png"
                      className="img-fluid"
                      alt="instagram"
                    />
                  </a>

                  {/* <a target="blank" href="https://in.linkedin.com/"><img src="https://nualoha.com/img/in.png" className="img-fluid" alt="linkedin" /></a> */}
                  {/* <a target="blank" href="https://www.instagram.com/?hl=en"><img src="https://nualoha.com/img/insta.png" className="img-fluid" alt="instagram" /></a>
                            <a target="blank" href="https://in.pinterest.com/"><img src="https://nualoha.com/img/pin.png" className="img-fluid" alt="pinterest" /></a> */}
                </span>
              </div>
            </div>
          </div>
        </div>

        <div className=" header">
          <div className="container align-items-center">
           <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <span class="navbar-brand"
    ><Link to='/'><img src="https://nualoha.com/img/nualoha.png" class="logo" alt="nualoha"/></Link>
   </span
  ><button
    aria-controls="responsive-navbar-nav"
    type="button"
    aria-label="Toggle navigation"
    class="navbar-toggler"
  >
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="navbar-collapse collapse" id="responsive-navbar-nav">
    <div class="navbar-nav">
        <Link to='/'>Home </Link>
        <Link to='/product_listings/'>Shop </Link>
    </div>
  </div>
</nav>

          </div>
        </div>
      </div>
    </div>
  );
};
export default HeaderBlock
