import React from "react";

import { Link, Redirect } from "react-router-dom";

const FooterBlock = () => {

  return (

    <div>

      <div className="footer">

        <div className="container">

          <div className="top">

            <div className="row">

              <div className="col-sm-9 links">

                <ul className="list-inline">

                  <li className="list-inline-item">

                    <Link to="/"> Home</Link>

                  </li>

                  <li className="list-inline-item">

                    <Link to="/products"> Shop</Link>

                  </li>

                  <li className="list-inline-item">

                    <Link to="/our-mission">Our Mission</Link>

                  </li>

                  <li className="list-inline-item">

                    <Link to="/our-standards">Our Standards</Link>

                  </li>

                  <li className="list-inline-item">

                    <Link to="/essential-oil-intro">

                      Intro To Essential Oil

                    </Link>

                  </li>

                  <li className="list-inline-item">

                    <Link to="/contact"> Contact</Link>

                  </li>

                  <li className="list-inline-item">

                    <Link to="/affiliate-program"> Affiliate Program</Link>

                  </li>

                </ul>

              </div>

              <div className="col-sm-3 text-right socail">

                <ul className="list-inline">

                  <li className="list-inline-item title">

                    <span>Follow us</span>

                  </li>

                  <li className="list-inline-item">

                    <a

                      target="blank"

                      href="https://twitter.com/NualohaNaturals"

                    >

                      <img

                        src="https://nualoha.com/img/twiteer.png"

                        width="34"

                        className="img-fluid"

                        alt="twitter"

                      />

                    </a>

                  </li>

                  <li className="list-inline-item">

                    <a

                      target="blank"

                      href="https://www.facebook.com/nualohanaturals/"

                    >

                      <img

                        src="https://nualoha.com/img/facebook-logo-in-circular-button-outlined-social-symbol.png"

                        width="34"

                        className="img-fluid"

                        alt="facebook"

                      />

                    </a>

                  </li>

                  <li className="list-inline-item">

                    <a

                      target="blank"

                      href="https://www.instagram.com/nualoha7/"

                    >

                      <img

                        src="https://nualoha.com/img/insta.png"

                        width="34"

                        className="img-fluid"

                        alt="instagram"

                      />

                    </a>

                  </li>

                </ul>

              </div>

            </div>

          </div>

          <div className="container">

            <div className="row">

              <div className=" linksP">

                <ul className="list-inline">

                  <li className="list-inline-item">

                    <Link to="/privacy-policy"> Privacy Policy</Link>

                  </li>

                  <li className="list-inline-item">

                    <Link to="/terms-conditons">Terms & Conditons</Link>

                  </li>

                </ul>

              </div>

            </div>

          </div>

          <div className="bottom">

            Copyright © 2019 NuAloha. All Rights Reserved. nualoha.com

          </div>

        </div>

      </div>

    </div>

  );

};



export default FooterBlock;


